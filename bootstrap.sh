mc alias set cyberrs-minio http://cyberrs-backbone-minio:9000 {{ .Values.minio.rootUser }} {{ .Values.minio.rootPassword }}
mc mb cyberrs-minio/redisbucket
mc event add cyberrs-minio/redisbucket arn:minio:sqs::_:redis --event put
