# backbone helm chart

deploys:
  * minio: bootstrapped with bucket `redisbucket`, which has [put](https://min.io/docs/minio/linux/reference/minio-mc/mc-event-add.html#supported-bucket-events) notifications to redis [enabled](https://min.io/docs/minio/linux/administration/monitoring/publish-events-to-redis.html#publish-events-to-redis)
  * redis: receives notifications from minio bucket `crs` on key `crs-put-events` (it's a redis [list](https://redis.io/docs/data-types/lists/)
  * traefik (optional)
  * traefik ingressRoute for minio (optional)

## continuous deployment

* `traefik` is configured and deployed here https://gitlab.com/cyberrs/infrastructure/traefik
* `main` branch is deployed to a [staging](https://gitlab.com/cyberrs/backbone/helm-chart/-/environments/12302913) environment
* `staging` deploys to https://minio.cyberrs.xyz
    - username: `admin`
    - password: `$MINIO_ROOT_PASSWORD` from https://gitlab.com/groups/cyberrs/-/settings/ci_cd
* `MRs` deploy to `review` environments, which are deleted when the MR is merged/closed
* non default `branches` without an associated MR deploy to `dev` environments, and are deleted after 1 hour of inactivity
